#!/bin/bash

# start ssh server
/etc/init.d/ssh start

# start yarn
$HADOOP_HOME/bin/yarn --daemon start resourcemanager
$HADOOP_HOME/bin/mapred --daemon start historyserver

# start spark history server
$SPARK_HOME/sbin/start-history-server.sh

# check if all daemons are running
STATUS=$(jps)
if [[ $STATUS == *"HistoryServer"* ]] && [[ $STATUS == *"JobHistoryServer"* ]] && [[ $STATUS == *"ResourceManager"* ]]; then
  echo "HistorySever, JobHistorySever and ResourceManager running"
  
  # keep container running
  tail -f /dev/null
fi


